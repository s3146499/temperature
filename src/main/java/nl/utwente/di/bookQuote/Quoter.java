package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> map = new HashMap<>();
    public  Quoter(){
        map.put("1",10.0);
        map.put("2",45.0);
        map.put("3",20.0);
        map.put("4",35.0);
        map.put("5",50.0);
        map.put("others",0.0);

    }
    public double getBookPrice(String thing) {
        return map.get(thing);
    }


}
